# Localization Functions

A set of functions which are available to the LocalizationManager to add customized localization strings.

## LocalizationManager:add_localized_strings(tbl)

Registers a table of keys localized strings.  
`tbl` A table of localized strings, where the key is the localization id, and the value is the localized string to fetch.  

```lua
-- Directly adding to the localized strings table
LocalizationManager:add_localized_strings({
  ["loc_example_test_string"] = "This is our localization test string!",
  ["loc_example_test_string2"] = "This is our localization test string!",
  ["loc_example_test_stringend"] = "This is the last string no comma is needed after this string!"
})

-- Automatically adding localized strings once the LocalizationManager has loaded
Hooks:Add("LocalizationManagerPostInit", "LocalizationManagerPostInit_LocExample", function(loc)

  loc:add_localized_strings({
    ["loc_example_test_string"] = "This is our localization test string!",
    ["loc_example_test_string2"] = "This is our localization test string!",
    ["loc_example_test_stringend"] = "This is the last string no comma is needed after this string!"
  })

  end)
```

## LocalizationManager:load_localization_file(file_path)

Loads a json formatted file and adds all keys and values to the localization table.  
`file_path` The file to be loaded, and registered in the localization manager.  

JSON

```json
{
  "loc_example_json_file" : "This is a localization string being loaded from JSON",
  "loc_example_json_file2" : "This is the last string no comma is needed after this string!"
}
```

Lua

```lua
LocalizationManager:load_localization_file("loc.json")
```
