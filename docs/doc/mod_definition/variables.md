# Mod related variables

These are global variables that

## RequiredScript

A variable containing the name and path of the Payday lua file that will be run, or was previously run.
This is a path as specified in Hooks or Pre-Hooks in the mod definition file.

## ModPath

A variable containing the path of the mod that is currently being loaded. This value should be cached if it is
intended to be used at some point in the future, such as in a Hook.  

```lua
"mods/my_example_mod/"
```

## LogsPath

A variable containing the path to the mods logs folder.  

```lua
"mods/logs/"
```

## SavePath

A variable containing the path to the save files folder.  

```lua
"mods/saves/"
```

## ModInstance

A variable containing the currently executed `BLTMod` instance. This value should be cached if it is
intended to be used at some point in the future, such as in a Hook.
