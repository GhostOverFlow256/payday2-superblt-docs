# Lua networking extensions

Various extension and helper functions for converting data types that may not normally be convertible into a string, into a string and back into their original type.

## Converting tables

### Table to string

```lua
LuaNetworking:TableToString(tbl)
```

Converts a table into a string which is able to sent through the Networking functions. It will only perform a shallow conversion, meaning it should not be used with nested tables.  
`tbl` The table to be converted into a string.  
`returns` A string representation of the table passed into the function.  

```lua
local tbl = { 1, 2, 3, 4, "hello world!" }
local str = Net:TableToString(tbl)
LuaNetworking:SendToPeers("NetworkTableStringTest", str)
```

### String to table

```lua
LuaNetworking:StringToTable(str)
```

Converts a formatted string into a table. Should be used for turning a networked table sent as a string back into its original table.  
`str` The formatted string to attempt to convert back into a table.  
`returns` A table containing the formatted data from `str`.  

```lua
local str = "key|value,key2|another value,another key|yet another value"
local tbl = LuaNetworking:StringToTable(str)
```

## Converting colors

### Color to string

```lua
LuaNetworking:ColourToString(col)
```

Converts a colour into a formatted string, which is able to be sent through the Networking functions.  
`col` The colour to be converted into a formatted string.  
`returns` A formatted string containing the colour data from `col`.  

```lua
local color = Color.red
local col_str = LuaNetworking:ColourToString(color)
LuaNetworking:SendToPeers("NetworkColourStringTest", col_str)
```

### String to color

```lua
LuaNetworking:StringToColour(str)
```

Converts a formatted string into a colour. Should be used for turning a networking colour sent as a string back to its original object.  
`str` The formatted string to attempt to convert back into a Color object.  
`returns` A Color object containing the original colour.  

```lua
  local col_str = "r:1.0000|g:0.2125|b:0.4568|a:1.0000"
  local color = LuaNetworking:StringToColor(col_str)
```

## Converting vectors

### Vector to string

```lua
LuaNetworking:Vector3ToString(v)
```

Converts a Vector3 to a formatted string.  
`vec` The Vector3 to convert to a string.  
`returns` A formatted string with the information of the input `vec`.  

```lua
local vec = Vector3(50, 0, 10)
LuaNetworking:Vector3ToString(vec)
```

### String to vector

```lua
LuaNetworking:StringToVector3(string)
```

Converts the formatted string `str` into a usable Vector3.  
`str` The formatted string to convert into a Vector3.  
`returns` A Vector3.  

```lua
local vec_str = "50.000000,25.456841,00.000000"
LuaNetworking:StringToVector3(vec_str)
```
